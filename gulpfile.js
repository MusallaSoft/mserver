var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var ts = require('gulp-typescript');
var path = require('path');

var tsProject = ts.createProject(path.resolve('./tsconfig.json'));

gulp.task('build', function () {
	var tsResult = gulp.src([
			'**/*.ts',
			'!typings/**/*.ts',
			'!node_modules/**/*.*'
		])
		.pipe(sourcemaps.init())
		.pipe(tsProject())
	return tsResult.js
		.pipe(sourcemaps.write('../maps'))
		.pipe(gulp.dest(path.resolve('./')));
});

gulp.task('default', ['build', 'watch']);