import * as express from "express";
const app = express();
const port = 3000;

app.get('/', (req, res) => res.send('Hello World!'));
app.use('/api/v1/test', (req, res) => {
	res.json({status: "SUCCESS", message: "TEST MESSAGE FROM /api/v1/test route" });
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))